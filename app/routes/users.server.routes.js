var users = require('../controllers/users.server.controller');
var passport = require('passport');

module.exports = function(app){
  app.route('/signup')
  	.get(users.renderSignup)
 	  .post(users.signup);

  app.route('/signin')
  	.get(users.renderSignin)
  	.post(passport.authenticate('local',{
  		successRedirect : '/',
  		failureRedirect : '/signin',
  		failureFlash : true
  	}));

  app.get('/signout',users.signout);

  app.get('/oauth/facebook', passport.authenticate('facebook', {
    scope:['email'],
    failureRedirect: '/signin'
  }));
  app.get('/oauth/facebook/callback', passport.authenticate('facebook',{
    failureRedirect:'/signin',
    successRedirect:'/'
  }));

  app.get('/oauth/twitter', passport.authenticate('twitter',{
    failureRedirect:'/signin'
  }));
  app.get('/oauth/twitter/callback', passport.authenticate('twitter',{
    failureRedirect:'/signin',
    successRedirect:'/'
  }));

  app.get('/oauth/google', passport.authenticate('google',{
    failureRedirect:'/signin',
    scope:[    'profile',    'email'    ],
    //scope:['https://www.googleapis.com/auth/plus.login',
    //'https://www.googleapis.com/auth/plus.profile.emails.read'],
    //scope:['https://www.googleapis.com/auth/userinfo.profile','https://www.googleapis.com/auth/userinfo.email'],
    //accessType:'offline',
    //approvalPrompt:'force'
  }));
  app.get('/oauth/google/callback', passport.authenticate('google', {
    failureRedirect:'/signin',
    successRedirect:'/'
  }));
};
