exports.render = function(req,res){
	if(req.session.lastVisit){
		console.log(req.session.lastVisit);
	}

	req.session.lastVisit = new Date();

	res.render('index',{
		title:'Hello World',
		user:JSON.stringify(req.user),
		userFullName : req.user ? req.user.fullName : null
	});
	//mongodb 버추얼은 ejs로 안넘어감
	//console.log(req.user);
};
