var User = require('mongoose').model('User');
var passport = require('passport');

////////////////
//inner methods
var getErrorMessage = function(err){
  var message = [];

  if(err.code){
    switch(err.code){
      case 11000:
      case 11001:
        message = 'Username already exists';
        break;
      default:
        message = 'Something went wrong';
    }
  } else {
    for(var errName in err.errors){
      if(err.errors[errName].message)
        message.push(err.errors[errName].message);
    }
  }
  return message;
};

/////////////////////////////
//user middlewares
exports.requiresLogin = function(req,res,next){
  if(!req.isAuthenticated()){
    return res.status(401).send({
      message:'User is not logined in'
    });
  }
  next();
};

/////////////////////////////
//express controller methods
exports.renderSignin = function(req,res){
  if(!req.user){
    res.render('signin',{
      title:'Sign-in Form',
      messages:req.flash('error') || req.flash('info')
    });
  } else {
    return res.redirect('/');
  }
};

exports.renderSignup = function(req,res){
  if(!req.user){
    res.render('signup', {
      title:'Sign-up Form',
      messages:req.flash('error')
    });
  } else {
    return res.redirect('/');
  }
};

exports.signup = function(req,res,next){
  if(!req.user){
    var user = new User(req.body);
    var message = null;

    user.provider = 'local';
    //console.log(user);

    user.save(function(err){
      if(err){
        var message = getErrorMessage(err);

        req.flash('error', message);
        return res.redirect('/signup');
      }

      req.login(user,function(err){
        if(err) return next(err);
        return res.redirect('/');
      });
    });
  } else {
    return res.redirect('/');
  }
};

exports.signout = function(req,res){
  req.logout();
  res.redirect('/');
};

////////////////////////////
// OAuth 사용자 생성처리 메소드
exports.saveOAuthUserProfile = function(req, profile, done){
  User.findOne({
    provider:profile.provider,
    providerId:profile.providerId
  },function(err, user){
    if(err){
      return done(err);
    } else{
      if(!user){
        var possibleUsername = profile.username  || ((profile.email) ? profile.email.split('@')[0] : '');
        User.findUniqueUsername(possibleUsername, null, function(availableUsername){
          profile.username = availableUsername;

          user = new User(profile);

          user.save(function(err){
            if(err) {
              //var message = getErrorMessage(err);
              console.log(err);
              //req.flash('error', message);
            }
            return done(err,user);
          });

        });
      } else {
        return done(err,user);
      }
    }
  });
};
