module.exports = {
  //개발 구성 옵션
  db:'mongodb://localhost/mean-book',
  sessionSecret : 'developmentSessionSecret',
  //facebook은 개발환경에서 localhost 만됨
  facebook : {
    clientID:'1641264452829437',
    clientSecret:'a9fd9b1456b0b7891056a2a962d7f5cd',
    callbackURL:'http://localhost:3000/oauth/facebook/callback'
  },
  //twitter는 개발환경에서 127.0.0.1 만됨
  twitter:{
    clientID:'l71oNcWQvZfD6lICH7GBbKw3M',
    clientSecret:'nt1x8ijkyCQJNoPgEFZv088ptWKsbpQr9q4P24lqQC3edXUhrv',
    callbackURL:'http://127.0.0.1:3000/oauth/twitter/callback'
  },
  //google은 개발환경에서 127.0.0.1 만됨
  google:{
    clientID:'307484753004-r2bmq64iv7auli1pvimb132pqnskpirm.apps.googleusercontent.com',
    clientSecret:'fVEGXYkEAc17giKA-D9NAOfE',
    callbackURL:'http://127.0.0.1:3000/oauth/google/callback'
  }
};
