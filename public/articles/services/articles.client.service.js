angular.module('articles')
.factory('Articles',['$resource',function($resource){
	return $resource('api/articles/:articlesId',{
		articleId:'@_id'
	},{
		update:{
			method:'PUT'
		}
	});
}]);